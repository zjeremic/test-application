package akka.first.app;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.first.app.actors.CrawlerClusterControllerActor;
 
import com.typesafe.config.ConfigFactory;

/**
 * @author Zoran Jeremic 2013-07-15
 */
public class MainClass {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
	 
		ActorSystem  _system = ActorSystem.create("ClusterSystem",
				ConfigFactory.load().getConfig("InextwebCrawler"));
	ActorRef  crawlerClusterControllerActor=_system.actorOf(Props.create(CrawlerClusterControllerActor.class),
			  "clusterController");
	try {
		Thread.sleep(1000000);
	} catch (InterruptedException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	}

}
