package akka.first.app.messages;
/**
 * @author Zoran Jeremic 2013-07-15
 */
public enum CrawlerJobType {
	STOPCRAWLER, OTHERJOB, REGISTERMANAGER
}
