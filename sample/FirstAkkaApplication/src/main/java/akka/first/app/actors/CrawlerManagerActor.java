package akka.first.app.actors;

import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

 

import java.util.logging.Logger;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.UntypedActor;
import akka.cluster.Cluster;
import akka.cluster.ClusterEvent.CurrentClusterState;
import akka.cluster.ClusterEvent.MemberUp;
import akka.cluster.Member;
import akka.cluster.MemberStatus;
import akka.first.app.messages.CrawlerJobType;
import akka.first.app.messages.GeneralJobMessage;

 

 

/**
 * @author zoran
 *
 */
public class CrawlerManagerActor  extends UntypedActor implements Serializable{
	Cluster cluster = Cluster.get(getContext().system());
	/**
	 * 
	 */
	private static final long serialVersionUID = 4405103053900481992L;
	private Logger logger = Logger.getLogger(CrawlerManagerActor.class
			.getName());
	 //subscribe to cluster changes, MemberUp
	@Override
	public void preStart() {
		cluster.subscribe(getSelf(), MemberUp.class);
	}
	 
	//re-subscribe when restart
	@Override
	public void postStop() {
		cluster.unsubscribe(getSelf());
	}
	@Override
	public void onReceive(Object message) throws Exception {
	 if (message instanceof CurrentClusterState) {
		 System.out.println("CurrentClusterState message received");
			CurrentClusterState state = (CurrentClusterState) message;
			for (Member member : state.getMembers()) {
				if (member.status().equals(MemberStatus.up())) {
		 		 registerManager(member);
			}
			}
			 
			} else if (message instanceof MemberUp) {
				 System.out.println("MemberUp message received");
				MemberUp mUp = (MemberUp) message;
				registerManager(mUp.member());
			} 
		
	}
	void registerManager(Member member) {
	
		GeneralJobMessage message=new GeneralJobMessage(CrawlerJobType.REGISTERMANAGER, null);
		getContext().actorSelection(member.address()+"/user/clusterController").tell(message, getSelf());
		}

 



 
}

