package akka.first.app.actors;

import akka.actor.Terminated;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

 


import akka.cluster.routing.AdaptiveLoadBalancingPool;
import akka.cluster.routing.ClusterRouterPool;
import akka.cluster.routing.ClusterRouterPoolSettings; 
import akka.cluster.routing.MixMetricsSelector;
import akka.first.app.messages.CrawlerJobType;
import akka.first.app.messages.GeneralJobMessage;
import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.UntypedActor;
 

/**
@Author Zoran Jeremic Nov 29, 2013
 */
public class CrawlerClusterControllerActor extends UntypedActor implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6582880018973450040L;
	List<ActorRef> managers = new ArrayList<ActorRef>();
	ActorRef crawlerManager;
	//ActorRef manager;
	
	public CrawlerClusterControllerActor(){
		int totalInstances = 100;
		int maxInstancesPerNode = 3;
		boolean allowLocalRoutees = true;
	//String routeesPath = "/user/factorialBackend";
		String useRole = null;
		AdaptiveLoadBalancingPool pool=new AdaptiveLoadBalancingPool(MixMetricsSelector.getInstance(), 0);
		ClusterRouterPoolSettings settings=new ClusterRouterPoolSettings(
				totalInstances, maxInstancesPerNode, allowLocalRoutees, useRole);
		 crawlerManager = getContext().actorOf(new ClusterRouterPool(pool, settings)
			.props(Props.create(CrawlerManagerActor.class)));
		//managers.add(crawlerManager);
	}

	@Override
	public void onReceive(Object message) throws Exception {
	  if(message instanceof GeneralJobMessage){
			if(((GeneralJobMessage) message).getJobType().equals(CrawlerJobType.REGISTERMANAGER)){
				getContext().watch(getSender());
				ActorRef sender=getSender();
				if(!managers.contains(sender)){
			      managers.add(sender);
			      System.out.println("registered manager:"+sender.hashCode()+" managers.size:"+managers.size());
					}else   System.out.println("already registered manager");
			}
		}
		  else if (message instanceof Terminated) {
		      Terminated terminated = (Terminated) message;
		      managers.remove(terminated.getActor());
		      System.out.println("removed manager");

		    } 
		
	}

}
